using System;
using System.Collections.Generic;
using System.Text;

namespace Abstraction.Database
{
    public interface IDatabaseReadAll<T>
    {
        public List<T> Get();
    }
}
