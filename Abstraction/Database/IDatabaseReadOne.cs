using System;
using System.Collections.Generic;
using System.Text;

namespace Abstraction.Database
{
    public interface IDatabaseReadOne<T>
    {
        public T Get(int id);
    }
}
