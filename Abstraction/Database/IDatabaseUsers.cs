using System;
using System.Collections.Generic;
using System.Text;

namespace Abstraction.Database
{
	public interface IDatabaseUsers : IDatabaseReadOne<Entity.IUsers>
	{
		public Entity.IUsers GetByUserName(string userName);
	}
}
