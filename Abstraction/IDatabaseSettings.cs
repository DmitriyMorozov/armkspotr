using System;
using System.Collections.Generic;
using System.Text;

namespace Abstraction
{
    public interface IDatabaseSetting
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string Location { get; set; }
        public string DB { get; set; }
    }
}
